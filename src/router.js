import Vue from "vue";
import Router from "vue-router";
import firebase from "firebase";

import Home from "./views/Home.vue";
import Login from "./views/Login";
// import SignUp from "./views/Signup";
import AppDetails from "./views/AppDetails";
import FrontApps from "./views/FrontApps";
import FrontLand from "./views/FrontLand";
import Team from "./views/Team";
import MemberDetails from "./views/MemberDetails";
import UserRedirect from "./views/UserRedirect";

Vue.use(Router);

const router = new Router({
  mode: "history",
  base: process.env.BASE_URL,
  routes: [
    {
      path: "*",
      redirect: "/apps"
    },
    {
      path: "/",
      redirect: "/apps"
    },
    {
      path: "/home",
      name: "home",
      component: Home,
      meta: {
        title: "Dashboard",
        requiresAuth: true
      }
    },
    {
      path: "/team",
      name: "Team",
      component: Team,
      meta: {
        title: "Team",
        requiresAuth: true
      }
    },
    {
      path: "/team/details",
      name: "MemberDetails",
      component: MemberDetails,
      props: true,
      meta: {
        title: "Member Details",
        requiresAuth: true
      }
    },
    {
      path: "/login",
      name: "Login",
      component: Login,
      meta: {
        title: "Login - AppLand"
      }
    },
    {
      path: "/sign-up",
      name: "SignUp",
      redirect: "/login"
    },
    // {
    //   path: "/sign-up",
    //   name: "SignUp",
    //   component: SignUp,
    //   meta: {
    //     title: "Sign Up - AppLand"
    //   }
    // },
    {
      path: "/apps/details",
      name: "AppDetails",
      component: AppDetails,
      props: true,
      meta: {
        title: "App Details",
        requiresAuth: true
      }
    },
    {
      path: "/apps",
      name: "FrontApps",
      component: FrontApps,
      meta: {
        title: "Our Apps",
        landing: true
      }
    },
    {
      path: "/apps/:appleid",
      name: "FrontLand",
      component: FrontLand,
      meta: {
        landing: true
      }
    },
    {
      path: "/oops",
      name: "UserRedirect",
      component: UserRedirect,
      meta: {
        title: "Permission Denied",
        errorpage: true
      }
    }
  ]
});

router.beforeEach((to, from, next) => {
  //dynamic title display from route meta 'title'
  if (to.meta.title) {
    document.title = to.meta.title;
  }
  //auth based route propagation check
  const currentUser = firebase.auth().currentUser;
  const requiresAuth = to.matched.some(record => record.meta.requiresAuth);

  if (!currentUser && requiresAuth) {
    next("login");
  } else next();
});

export default router;
