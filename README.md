# AppLand - InfancyIT iOS Apps' Dynamic Landing

Dynamic app landing page for infancy IT's iOS apps. Contains admin panel to manage team and apps appearances.

Made with Vue.js & Firebase's Auth + RealtimeDB and a lot of vanilla JS code.

### Static build is hosted on netlify. Check [here](https://appland.netlify.com/). 

## Project setup
```
yarn install
```

### Compiles and hot-reloads for development
```
yarn serve
```

### Compiles and minifies for production
```
yarn build
```

### Run your tests
```
yarn test
```

### Lints and fixes files
```
yarn lint
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).
