import Vue from "vue";
import BootstrapVue from "bootstrap-vue";
import firebase from "firebase";
import VueProgressBar from "vue-progressbar";
import { library } from "@fortawesome/fontawesome-svg-core";
import { faMobileAlt } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/vue-fontawesome";
import Toasted from "vue-toasted";
import Gravatar from "vue-gravatar";
import App from "./App.vue";
import router from "./router";
import "bootstrap";
import "bootstrap/dist/css/bootstrap.css";
import "bootstrap-vue/dist/bootstrap-vue.css";
require("vue2-animate/dist/vue2-animate.min.css");

Vue.config.productionTip = false;
Vue.use(BootstrapVue);

library.add(faMobileAlt);
Vue.component("font-awesome-icon", FontAwesomeIcon);
Vue.component("v-gravatar", Gravatar);

const progressOptions = {
  color: "#008080",
  failedColor: "#FFA500",
  thickness: "10px",
  transition: {
    speed: "0.2s",
    opacity: "0.6s",
    termination: 300
  },
  autoRevert: true,
  location: "top",
  inverse: false
};
Vue.use(VueProgressBar, progressOptions);

Vue.use(Toasted, {
  iconPack: "fontawesome",
  duration: 2500,
  theme: "outline"
});

let app = "";

var config = {
  apiKey: "AIzaSyAmjdfD2_2dTmjD95tbsxhjfwZGiT2YuKc",
  authDomain: "app-land.firebaseapp.com",
  databaseURL: "https://app-land.firebaseio.com",
  projectId: "app-land",
  storageBucket: "app-land.appspot.com",
  messagingSenderId: "764133494575"
};
firebase.initializeApp(config);

firebase.auth().onAuthStateChanged(() => {
  if (!app) {
    app = new Vue({
      router,
      render: h => h(App)
    }).$mount("#app");
  }
});
